import pytest
from django.test import Client
from django.contrib.auth.models import User
from mixer.backend.django import mixer
from django.contrib import auth

pytestmark = pytest.mark.django_db


class TestIndexView:
    def test_anonymous(self):
        c = Client()
        resp = c.get('/', follow=True).redirect_chain
        test_chain = [('/login?next=/', 302), ('/login/?next=%2F', 301)]
        assert resp == test_chain, 'Should redirect anonymous users to login'

    def test_logged(self):
        c = Client()
        player = mixer.blend('playarena_scheduler.Player')
        User.objects.create_user(username='testuser',
                                 password='testpassword',
                                 player=player)
        logged = c.login(username='testuser', password='testpassword')
        response = c.get('/')
        assert logged is True, 'Should login user'
        assert response.status_code == 200, ('Should allow authenticated '
                                             'user view page')


class TestLoginView:
    def test_anonymous(self):
        c = Client()
        response = c.get('/login/')
        assert response.status_code == 200, 'Should load login page for anon'

    def test_logging(self):
        c = Client()
        player = mixer.blend('playarena_scheduler.Player')
        User.objects.create_user(username='testuser',
                                 password='testpassword',
                                 player=player)
        login_resp = c.post('/login/', {'username': 'testuser',
                                        'password': 'testpassword'},
                            follow=True)
        assert login_resp.redirect_chain == [('/', 302)], ('Should load '
                                                           'website')

    def test_wrong_password(self):
        c = Client()
        player = mixer.blend('playarena_scheduler.Player')
        User.objects.create_user(username='testuser',
                                 password='testpassword',
                                 player=player)
        login_resp = bytes.decode(c.post('/login/', {'username': 'testuser',
                                         'password': 'testpwssword'}).content)
        assert 'Wrong password or username' in login_resp, ('Should contain'
                                                            'information about'
                                                            'wrong password')


class TestLogoutView:
    def test_logging_out(self):
        c = Client()
        player = mixer.blend('playarena_scheduler.Player')
        User.objects.create_user(username='testuser',
                                 password='testpassword',
                                 player=player)
        c.post('/login/', {'username': 'testuser',
                                       'password': 'testpassword'})
        user = auth.get_user(c)
        assert user.is_authenticated() is True, 'Should be logged'
        login_view_response = bytes.decode(c.get('/logout/',
                                           follow=True).content)
        user = auth.get_user(c)
        assert user.is_authenticated() is False, 'Should be logged out'
        assert 'Succesfully logout' in login_view_response
