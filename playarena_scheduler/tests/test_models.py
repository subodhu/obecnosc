import datetime

import pytest
from mixer.backend.django import mixer

from ..models import Date, Time

pytestmark = pytest.mark.django_db


class TestPositon:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Position')
        assert obj.pk == 1, 'Should create a Position instance'

    def test___str__(self):
        obj = mixer.blend('playarena_scheduler.Position', name='Forward')
        string_representation = obj.__str__()
        assert string_representation == 'Forward', ('Should return proper'
                                                    ' Position name')


class TestAdditionalPerk:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.AdditionalPerk')
        assert obj.pk == 1, 'Should create a AdditionalPerk instance'

    def test___str__(self):
        obj = mixer.blend('playarena_scheduler.AdditionalPerk', name='capitan')
        string_representation = obj.__str__()
        assert string_representation == 'capitan', ('Should return proper'
                                                    'AdditionalPerk name')


class TestTeam:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Team')
        assert obj.pk == 1, 'Should create a Team instance'

    def test___str__(self):
        obj = mixer.blend('playarena_scheduler.Team', name='KS Styropian')
        string_representation = obj.__str__()
        assert string_representation == 'KS Styropian', ('Should return proper'
                                                         'Team name')


class TestPlayer:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Player')
        assert obj.pk == 1, 'Should create a Player instance'

    def test___str__(self):
        obj = mixer.blend('playarena_scheduler.Player',
                          first_name='Franciszek',
                          last_name='Madej',
                          kit_number=29)
        string_representation = obj.__str__()
        assert string_representation == '29 - F. Madej', ('Should return'
                                                          'properly formatted'
                                                          'Player name')


class TestDate:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Date')
        assert obj.pk == 1, 'Should create a Date instance'

    def test___str__(self):
        date = datetime.date(2017, 7, 1)
        obj = mixer.blend('playarena_scheduler.Date', date=date)
        string_representation = obj.__str__()
        assert string_representation == '2017-07-01', ('Should return properly'
                                                       'formatted Date')

    def test_save(self):
        date = datetime.date(2017, 7, 1)
        mixer.blend('playarena_scheduler.Date', date=date)
        mixer.blend('playarena_scheduler.Date', date=date)
        assert Date.objects.all().count() == 1, ('Should not allow creation '
                                                 'of more than one instance '
                                                 'of Data per day')


class TestTime:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Time')
        assert obj.pk == 1, 'Should create a Time instance'

    def test___str__(self):
        time = datetime.time(17, 30)
        obj = mixer.blend('playarena_scheduler.Time', time=time)
        string_representation = obj.__str__()
        assert string_representation == '17:30:00', ('Should return properly'
                                                     'formatted Time')

    def test_save(self):
        time = datetime.time(17, 30)
        mixer.blend('playarena_scheduler.Time', time=time)
        mixer.blend('playarena_scheduler.Time', time=time)
        assert Time.objects.all().count() == 1, ('Should not allow creation '
                                                 'of more than one instance '
                                                 'of Time per specific hour')


class TestAvailability:
    def test_model(self):
        obj = mixer.blend('playarena_scheduler.Availability')
        assert obj.pk == 1, 'Should create a Availability instance'

    def test___str__(self):
        player = mixer.blend('playarena_scheduler.Player')
        player_str = player.__str__()

        date = mixer.blend('playarena_scheduler.Date')
        date_str = date.__str__()

        time = mixer.blend('playarena_scheduler.Time')
        time_str = time.__str__()

        obj = mixer.blend('playarena_scheduler.Availability',
                          time=time, date=date, player=player)
        string_representation = obj.__str__()
        if obj.available:
            word = ' is on '
        else:
            word = ' isn\'t on '
        string_test = player_str + word + date_str + ' ' + time_str
        assert string_representation == string_test, ('Should return properly'
                                                      ' formatted Availability'
                                                      ' class representation')
