import datetime

from mixer.backend.django import mixer

from ...models import Date, Time
from .helper import GraphQLTestCase


class TestCreateAvailability(GraphQLTestCase):
    def test_create_availability(self):
        date = Date(date=datetime.datetime.now().date())
        date.save()
        time = Time(time=datetime.datetime.now().time())
        time.save()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date.__str__().replace('-', '.', 2),
            'time': time.__str__(),
            'available': True
        }
        expected = {
            'createAvailability': {
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

    def test_create_availability_when_date_does_not_exist(self):
        date = datetime.datetime.now().__str__()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date[0:10].replace('-', '.', 2),
            'time': date[11:],
            'available': True
        }
        expected = {
            'createAvailability': {
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

    def test_edit_availability(self):
        date = datetime.datetime.now().__str__()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                availability {
                    available
                }
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date[0:10].replace('-', '.', 2),
            'time': date[11:],
            'available': True
        }
        expected = {
            'createAvailability': {
                'availability': {
                    'available': True
                },
                'success': True
            }
        }

        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

        # now check if you can edit it
        input_dict['available'] = False
        expected = {
            'createAvailability': {
                'availability': {
                    'available': False
                },
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)
