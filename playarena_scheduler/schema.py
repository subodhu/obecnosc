from datetime import datetime

import graphene
from graphene_django.types import DjangoObjectType
from playarena_scheduler.models import (AdditionalPerk, Availability, Date,
                                        Player, Position, Team, Time)


class PlayerType(DjangoObjectType):
    is_captain = graphene.Boolean(source='is_captain')

    class Meta:
        model = Player


class PositionType(DjangoObjectType):
    class Meta:
        model = Position


class TeamType(DjangoObjectType):
    class Meta:
        model = Team


class AvailabilityType(DjangoObjectType):
    class Meta:
        model = Availability


class DateType(DjangoObjectType):
    class Meta:
        model = Date


class TimeType(DjangoObjectType):
    class Meta:
        model = Time


class AdditionalPerkType(DjangoObjectType):
    class Meta:
        model = AdditionalPerk


class CreateAvailability(graphene.Mutation):
    class Input:
        player_kit_number = graphene.Int()
        date = graphene.String()
        time = graphene.String()
        available = graphene.Boolean()

    success = graphene.Boolean()
    availability = graphene.Field(lambda: AvailabilityType)

    @staticmethod
    def mutate(root, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()

        try:
            time_obj = Time.objects.get(time=args.get('time'))
        except Time.DoesNotExist:
            time_obj = Time(time=args.get('time'))
            time_obj.save()

        player = Player.objects.get(kit_number=args.get('player_kit_number'))
        available = args.get('available')

        try:
            availability_obj = Availability.objects.get(date=date_obj,
                                                        time=time_obj,
                                                        player=player)
            availability_obj.available = available
        except Availability.DoesNotExist:
            availability_obj = Availability(date=date_obj,
                                            time=time_obj,
                                            player=player,
                                            available=available)
            availability_obj.available = available
        availability_obj.save()
        success = True
        return CreateAvailability(availability=availability_obj,
                                  success=success)


class MyMutations(graphene.ObjectType):
    create_availability = CreateAvailability.Field()


class Query(graphene.AbstractType):
    daily_input_view = graphene.List(AvailabilityType,
                                     kit_number=graphene.Int(),
                                     date=graphene.String())
    daily_output_view = graphene.List(AvailabilityType,
                                      date=graphene.String())
    user_info = graphene.Field(PlayerType)
    team_info = graphene.Field(TeamType)

    def resolve_daily_input_view(self, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        kit_number = args.get('kit_number')
        try:
            player = Player.objects.get(kit_number=kit_number)
        except Player.DoesNotExist:
            return []
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()
        return Availability.objects.filter(date=date_obj,
                                           player=player)

    def resolve_daily_output_view(self, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()
        return Availability.objects.filter(date=date_obj)

    def resolve_user_info(self, args, context, info):
        return context.user.player

    def resolve_team_info(self, args, context, info):
        return context.user.player.team
