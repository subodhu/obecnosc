from django.contrib.auth import login as login_session
from django.contrib.auth import logout as logout_session
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.views.decorators.csrf import ensure_csrf_cookie


@ensure_csrf_cookie
@login_required(login_url='/login')
def index(request):
    return render(request, 'index.html')


def login(request, message_type=None):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login_session(request, user)
        return redirect('index')
    elif message_type == 'session_ended':
        return render(request, 'login.html',
                      {'error': 'Succesfully logout',
                       'label': 'primary'})
    elif username is None and password is None:
        return render(request, 'login.html')
    else:
        return render(request, 'login.html',
                      {'error': 'Wrong password or username',
                       'label': 'error'})


def logout(request):
    logout_session(request)
    return redirect('/login/session_ended')
