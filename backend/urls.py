"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.mixins import LoginRequiredMixin

import backend.views
from graphene_django.views import GraphQLView


class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
    login_url = '/login'


urlpatterns = [
    # url(r'^', include('playarena_scheduler.urls')),
    url(r'^login/?(?P<message_type>\w+?)$', backend.views.login, name='login'),
    url(r'^login/$', backend.views.login, name='login'),
    url(r'^logout/$', backend.views.logout, name='logout'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', backend.views.index, name='index'),
    url(r'^api', PrivateGraphQLView.as_view(graphiql=True)),
]
