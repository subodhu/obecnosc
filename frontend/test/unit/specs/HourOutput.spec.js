import HourOutputController from '../../../components/UI/HourOutput/Controller.vue'
import HourOutputView from '../../../components/UI/HourOutput/View.vue'
import apolloProvider from '../apollo-mock.js'
import store from '../../../store/index.js'
import {mount} from 'avoriaz'
import moment from 'moment'

describe('HourOutput/Controller.vue', () => {
  it('it mounts', () => {
    const wrapper = mount(HourOutputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({selectedDate: testDate})
    expect(wrapper.vm.selectedDay).to.equal(testDate.format('ddd, D.MM'))
    expect(wrapper.vm.GraphQLFormattedDate).to.equal(testDate.format('YYYY.MM.DD'))
  })
})

describe('HourOutput/View.vue', () => {
  it('it mounts', () => {
    const wrapper = mount(HourOutputView)
    const hourList = [
      {
        'hour': '08:30',
        'playersList': [
          {
            'available': true,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '10:00',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '11:30',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '13:00',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '14:30',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '16:00',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '17:30',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '19:00',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }, {
        'hour': '20:30',
        'playersList': [
          {
            'available': false,
            'firstName': 'Franciszek',
            'lastName': 'Madej',
            'kitNumber': 29,
            'position': 'Forward'
          }
        ]
      }
    ]
    const groupedPlayersForFirstHour = {
      Forward: [
        {
          available: true,
          firstName: 'Franciszek',
          lastName: 'Madej',
          kitNumber: 29,
          position: 'Forward'
        }
      ]
    }
    wrapper.setProps({hourList: hourList})
    expect(wrapper.vm.$props.hourList).to.equal(hourList)
    expect(wrapper.vm.groupPlayersByPositon(hourList[0].playersList)).to.deep.equal(groupedPlayersForFirstHour)
    expect(wrapper.vm.getAvailablePlayersCount(hourList[0].playersList)).to.equal(1)
  })
})
