import Vue from 'vue'
import Vuex from 'vuex'
import AppHeader from '../../../components/AppHeader.vue'
import router from '../../../router.js'
import {mount} from 'avoriaz'
import moment from 'moment'

describe('AppHeader.vue', () => {
  Vue.use(Vuex)
  let mutations
  let store

  beforeEach(() => {
    mutations = {
      updateSelectedDate: sinon.stub()
    };
    store = new Vuex.Store({
      state: {},
      mutations
    })
  })
  it('mounts', () => {
    const wrapper = mount(AppHeader, {router, store})
    expect(wrapper.vm.selectedDate).to.equal(moment().format('YYYY-MM-DD'))
  })

  it('it triggers store mutation when date changes', () => {
    const wrapper = mount(AppHeader, {router, store})
    wrapper.vm.updateSelectedDate()
    expect(mutations.updateSelectedDate.calledOnce).to.equal(true);
  })
})
