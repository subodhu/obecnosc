import ApolloClient from 'apollo-client';
import VueApollo from 'vue-apollo'
import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import { mockNetworkInterfaceWithSchema } from 'apollo-test-utils';

const typeDefs = `
  type Team {
    id: Int
    name: String
    startHour: String
    endHour: String
    interval: String
  }

  type Position {
    id: Int
    name: String
  }

  type AdditionalPerk {
    id: Int
    name: String
  }

  type Player {
    id: Int
    firstName: String
    lastName: String
    kitNumber: Int
    position: Position
    team: Team
    additionalPerks: [AdditionalPerk]
  }

  type Time {
    id: Int
    time: String
  }

  type Date {
    id: Int
    date: String
  }

  type Availability {
    id: Int
    available: Boolean
    date: Date
    time: Time
    player: Player
  }

  type Query {
    userInfo: Player
    teamInfo: Team
    dailyInputView(kitNumber: Int, date: String): [Availability]
    dailyOutputView(date: String): [Availability]
  }

  query dailyInputView($kitNumber: Int, $date: String) {
    dailyInputView(kitNumber: $kitNumber, date: $date) {
      available
      time {
        time
      }
    }
  }

  query dailyOutputView($date: String) {
                dailyOutputView(date: $date) {
                    available
                    time {
                       time
                    }
                    player {
                        lastName
                        kitNumber
                    }
                }
            }
`;

const schema = makeExecutableSchema({ typeDefs });
addMockFunctionsToSchema({ schema });

const mockNetworkInterface = mockNetworkInterfaceWithSchema({ schema });

const client = new ApolloClient({
  networkInterface: mockNetworkInterface
})

const apolloProvider = new VueApollo({
  defaultClient: client
})

export default apolloProvider
