import moment from 'moment'

const state = {
  teamInfo: {
    name: 'Team Name',
    startHour: '08:30:00',
    endHour: '20:30:00',
    interval: '01:30:00',
    emblem: '',
    startOnMonday: true
  },
  userInfo: {
    firstName: '',
    lastName: '',
    kitNumber: 1
  },
  userSettings: {
    selectedDate: moment()
  }
}

export default state
