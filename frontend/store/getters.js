import moment from 'moment'

const getters = {
  teamInterval: state => {
    if (state.teamInfo.interval !== '') {
      const hoursMatch = state.teamInfo.interval.match(/([0-9]+:)/)[1]
      const hours = Number(hoursMatch.substr(0, hoursMatch.length - 1))
      const minutesMatch = state.teamInfo.interval.match(/(:[0-9]+:)/)[1]
      const minutes = Number(minutesMatch.substr(1, hoursMatch.length - 1))
      return { hours, minutes }
    } else {
      return { hours: 0, minutes: 0 }
    }
  },
  teamHours: state => {
    const startHour = state.teamInfo.startHour
    const endHour = state.teamInfo.endHour
    return { startHour, endHour }
  },
  teamDetails: state => {
    const name = state.teamInfo.name
    const startOnMonday = state.teamInfo.startOnMonday
    return { name, startOnMonday }
  },
  userName: state => {
    const firstName = state.userInfo.firstName
    const lastName = state.userInfo.lastName
    return { firstName, lastName }
  },
  userNumber: state => {
    return state.userInfo.kitNumber
  },
  isCaptain: state => {
    return state.userInfo.isCaptain
  },
  selectedDate: state => {
    return moment(state.userSettings.selectedDate)
  }
}

export default getters
