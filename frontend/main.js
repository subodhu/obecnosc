import Vue from 'vue'
import App from './App.vue'
import apolloProvider from './apollo.js'
import store from './store/index.js'
import router from './router.js'

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  apolloProvider,
  store,
  router,
  render: h => h(App)
})
