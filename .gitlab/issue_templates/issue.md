Before submitting an issue I have first:

[] read the README.md
[] searched for similar already existing issue
*(if you have performed all the above, remove the paragraph and continue*
*describing the issue with template below)*

Issue

*Enter brief description of Issue here*

Context

Obecność version: (X.Y)
Any additional info:

*Please include any relevant log that could serve to better understand*
*the source of your issue*

Problem description

*what were you doing (simple push, merge request, MR with fork, ...)*
*what was expected*
*what occurred finally*
