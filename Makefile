# Use development settings for running django dev server.
export DJANGO_SETTINGS_MODULE=backend.settings.local

# Initializes virtual environment with basic requirements.
prod:
	pip install -r requirements/production.txt
	yarn

# Installs development requirements.
dev:
	pip install -r requirements/local.txt
	yarn

# Runs development server.
# This step depends on `make dev`, however dependency is excluded to speed up dev server startup.
run:
	yarn run dev & python ./manage.py runserver

# Creates migrations and migrates database.
# This step depends on `make dev`, however dependency is excluded to speed up dev server startup.
migrate:
	python ./manage.py makemigrations
	python ./manage.py migrate

# Builds files for distribution which will be placed in /static/dist
build: clean prod migrate
	yarn run build
	./manage.py collectstatic
	echo "Now run ./start_gunicorn.sh"

# Cleans up folder by removing virtual environment, node modules and generated files.
clean:
	rm -rf node_modules
	rm -rf static/dist

# Run linter
lint:
	yarn run lint --silent
